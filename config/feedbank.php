<?php

return [
    'buckets' => [
        'videos' => 'https://s3-ap-southeast-1.amazonaws.com/videos.feedbank.com',
        'images' => 'https://s3-ap-southeast-1.amazonaws.com/images.feedbank.com',
    ],
];
