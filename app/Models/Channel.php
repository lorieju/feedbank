<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'description',
        'image_filename',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function getImage()
    {
        if(!$this->image_filename){
            return config('feedbank.buckets.images') . '/profile/default-img.png';
        }

        return config('feedbank.buckets.images') . '/profile'. $this->image_filename;
    }
}
